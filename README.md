# README #

Osram Lightify Mini Switch driver for Hubitat Elevation smart home hub.

Add this to Hubitat Admin: "Advanced" -> "Drivers Code" -> "New Driver" -> copy source code and save.  
Then configure your Mini Switch to use this driver: "Devices" -> select <Specific Switch> -> "Device information" section -> "Type" selectobox
