/**
 *  Copyright 2020 A.Keinaste (keints)
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 *
 *  Original source by G.Brown: 
 *  https://github.com/MorkOz/Hubitat/blob/master/Drivers/zemismart-zigbee-button-remote-driver.groovy
 *
 *  Version Author               Note
 *  0.1     A.Keinaste (keints)  Initial release
 *
 */
import hubitat.zigbee.zcl.DataType

metadata {
    definition (name: "Osram Lightify Mini Switch", namespace: "akeints", author: "A.Keinaste") {
    capability "Actuator"
    capability "PushableButton"
    capability "HoldableButton"
    capability "Configuration"
    capability "Refresh"
    capability "Sensor"

        fingerprint inClusters: "0000,0001,0006", outClusters: "0019", manufacturer: "_TYZB02_key8kk7r", model: "TS0041"
        fingerprint inClusters: "0000,0001,0006", outClusters: "0019", manufacturer: "_TYZB02_key8kk7r", model: "TS0042"
        fingerprint inClusters: "0000,0001,0006", outClusters: "0019", manufacturer: "_TYZB02_key8kk7r", model: "TS0043"		
    }

    preferences {
        input name: "debugEnable", type: "bool", title: "Enable debug logging", defaultValue: false
    }

}

private sendButtonNumber() {
    def remoteModel = device.getDataValue("model")
    switch(remoteModel){
        case "TS0041":
            sendEvent(name: "numberOfButtons", value: 1, isStateChange: true)
            break
        case "TS0042":
            sendEvent(name: "numberOfButtons", value: 2, isStateChange: true)
            break
        case "TS0043":
            sendEvent(name: "numberOfButtons", value: 3, isStateChange: true)
            break
    }
}

def installed() {
    sendButtonNumber
    state.start = now()
}

def updated() {
    sendButtonNumber
}

def refresh() {
    // read battery level attributes
    return zigbee.readAttribute(0x0001, 0x0020) + zigbee.configureReporting(0x0001, 0x0020, 0x20, 3600, 21600, 0x01)
    // this device doesn't support 0021
    zigbee.readAttribute(0x0001, 0x0021)
}

def configure() {
    sendButtonNumber
    
    def configCmds = []
    for (int endpoint=1; endpoint<=3; endpoint++) {
        def list = ["0006", "0001", "0000"]
        // the config to the switch
        for (cluster in list) {
            configCmds.add("zdo bind 0x${device.deviceNetworkId} 0x0${endpoint} 0x01 0x${cluster} {${device.zigbeeId}} {}")
        }
    }
    return configCmds
}

def parse(String description) {
    if ((description?.startsWith("catchall:")) || (description?.startsWith("read attr -"))) {
        def parsedMap = zigbee.parseDescriptionAsMap(description)
        /*if (debugEnable){
            log.debug("Message Map: '$parsedMap'")
        }*/
        switch(parsedMap.sourceEndpoint) {
            case "03": 
                button = "3" // center button
                break
            case "02": 
                button = "2" // small button
                break
            case "01": 
                button = "1" // large button
                break
        }
        switch(parsedMap.command) {
            case "01": 
                name = "pushed" 
                break
            case "03": 
                name = "released" 
                break
            case "05": 
                name = "held" 
                break
            default:
                name = "pushed" 
                break
        }
        if (debugEnable){
            log.debug("Mini switch event: button '$button' action '$name'")
        }
        sendEvent(name: name, value: button, descriptionText: "Button $button was $name",isStateChange:true)
    }
    return
}